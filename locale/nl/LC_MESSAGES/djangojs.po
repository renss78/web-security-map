# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-22 13:09+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: static/admin/js/actions.js static/admin/js/actions.min.js
msgid "%(sel)s of %(cnt)s selected"
msgid_plural "%(sel)s of %(cnt)s selected"
msgstr[0] ""
msgstr[1] ""

#: static/admin/js/actions.js static/admin/js/actions.min.js
msgid ""
"You have unsaved changes on individual editable fields. If you run an "
"action, your unsaved changes will be lost."
msgstr ""

#: static/admin/js/actions.js static/admin/js/actions.min.js
msgid ""
"You have selected an action, but you haven't saved your changes to "
"individual fields yet. Please click OK to save. You'll need to re-run the "
"action."
msgstr ""

#: static/admin/js/actions.js static/admin/js/actions.min.js
msgid ""
"You have selected an action, and you haven't made any changes on individual "
"fields. You're probably looking for the Go button rather than the Save "
"button."
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "January"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "February"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "March"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "April"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "May"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "June"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "July"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "August"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "September"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "October"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "November"
msgstr ""

#: static/admin/js/calendar.js static/js/graphs.js
msgid "December"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Sunday"
msgid "S"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Monday"
msgid "M"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Tuesday"
msgid "T"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Wednesday"
msgid "W"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Thursday"
msgid "T"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Friday"
msgid "F"
msgstr ""

#: static/admin/js/calendar.js
msgctxt "one letter Saturday"
msgid "S"
msgstr ""

#: static/admin/js/collapse.js static/admin/js/collapse.min.js
msgid "Show"
msgstr ""

#: static/admin/js/collapse.js static/admin/js/collapse.min.js
msgid "Hide"
msgstr ""

#: static/jet/js/build/bundle.min.js
msgid "'"
msgstr ""

#: static/js/websecmap.js static/js/map.js static/js/views.js
msgid "View Full Screen"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Search organization"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Perfect"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Good"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Mediocre"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Bad"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Unknown"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "legend_basic_security"
msgstr ""

#: static/js/websecmap.js static/js/map.js
msgid "Zoom to show all data on this map."
msgstr ""

#: static/js/graphs.js
msgid "Jan"
msgstr ""

#: static/js/graphs.js
msgid "Feb"
msgstr ""

#: static/js/graphs.js
msgid "Mar"
msgstr ""

#: static/js/graphs.js
msgid "Apr"
msgstr ""

#: static/js/graphs.js
msgid "Jun"
msgstr ""

#: static/js/graphs.js
msgid "Jul"
msgstr ""

#: static/js/graphs.js
msgid "Aug"
msgstr ""

#: static/js/graphs.js
msgid "Sept"
msgstr ""

#: static/js/graphs.js
msgid "Oct"
msgstr ""

#: static/js/graphs.js
msgid "Nov"
msgstr ""

#: static/js/graphs.js
msgid "Dec"
msgstr ""

#: static/js/graphs.js
msgid "low"
msgstr ""

#: static/js/graphs.js
msgid "medium"
msgstr ""

#: static/js/graphs.js
msgid "high"
msgstr ""

#: static/js/script.js
msgid "2 weeks ago"
msgstr ""

#: static/js/script.js
msgid "3 weeks ago"
msgstr ""

#: static/js/script.js
msgid "1 month ago"
msgstr ""

#: static/js/script.js
msgid "2 months ago"
msgstr ""

#: static/js/script.js
msgid "3 months ago"
msgstr ""

#: static/js/script.js
msgid "7 days ago"
msgstr ""

#: static/js/script.js
msgid "now"
msgstr ""

#: static/js/script.js
msgid "Has a secure equivalent, which wasn't so in the past."
msgstr ""

#: static/js/script.js
msgid ""
"Site does not redirect to secure url, and has no secure alternative on a "
"standard port."
msgstr ""

#: static/js/script.js
msgid ""
"Redirects to a secure site, while a secure counterpart on the standard port "
"is missing."
msgstr ""

#: static/js/script.js
msgid "Broken Transport Security, rated F"
msgstr ""

#: static/js/script.js
msgid "Certificate not valid for domain name."
msgstr ""

#: static/js/script.js
msgid "Less than optimal Transport Security, rated C."
msgstr ""

#: static/js/script.js
msgid "Less than optimal Transport Security, rated B."
msgstr ""

#: static/js/script.js
msgid "Good Transport Security, rated A-."
msgstr ""

#: static/js/script.js
msgid "Good Transport Security, rated A."
msgstr ""

#: static/js/script.js
msgid "Perfect Transport Security, rated A+."
msgstr ""

#: static/js/script.js
msgid "X-Content-Type-Options header present."
msgstr ""

#: static/js/script.js
msgid "Missing X-Content-Type-Options header."
msgstr ""

#: static/js/script.js
msgid "X-XSS-Protection header present."
msgstr ""

#: static/js/script.js
msgid "Missing X-XSS-Protection header."
msgstr ""

#: static/js/script.js
msgid "X-Frame-Options header present."
msgstr ""

#: static/js/script.js
msgid "Missing X-Frame-Options header."
msgstr ""

#: static/js/script.js
msgid "Strict-Transport-Security header present."
msgstr ""

#: static/js/script.js
msgid "Missing Strict-Transport-Security header."
msgstr ""

#: static/js/script.js
msgid ""
"Missing Strict-Transport-Security header. Offers no insecure alternative "
"service."
msgstr ""

#: static/js/script.js
msgid "DNSSEC is incorrectly or not configured (errors found)."
msgstr ""

#: static/js/script.js
msgid "DNSSEC seems to be implemented sufficiently."
msgstr ""

#: static/js/script.js
msgid "FTP Server only supports insecure SSL protocol."
msgstr ""

#: static/js/script.js
msgid "FTP Server does not configured to show if encryption is available."
msgstr ""

#: static/js/script.js
msgid "FTP Server supports TLS encryption protocol."
msgstr ""

#: static/js/script.js
msgid "FTP Server does not support encrypted transport or has protocol issues."
msgstr ""

#: static/js/script.js
msgid ""
"An FTP connection could not be established properly. Not possible to verify "
"encryption."
msgstr ""

#: static/js/script.js
msgid "not trusted"
msgstr ""

#: static/js/script.js
msgid "trusted"
msgstr ""

#: static/js/script.js
msgid "Certificate is not trusted."
msgstr ""

#: static/js/script.js
msgid "Certificate is trusted."
msgstr ""

#: static/js/script.js
msgid ""
"Content-Security-Policy header found, which covers the security aspect of "
"the X-Frame-Options header."
msgstr ""

#: static/js/script.js
msgid ""
"Content-Security-Policy header found, which covers the security aspect of "
"the X-XSS-Protection header."
msgstr ""

#: static/js/script.js
msgid "report_header_tls_qualys"
msgstr ""

#: static/js/script.js
msgid "report_header_plain_https"
msgstr ""

#: static/js/script.js
msgid "report_header_http_security_header_x_xss_protection"
msgstr ""

#: static/js/script.js
msgid "report_header_http_security_header_x_frame_options"
msgstr ""

#: static/js/script.js
msgid "report_header_http_security_header_x_content_type_options"
msgstr ""

#: static/js/script.js
msgid "report_header_http_security_header_strict_transport_security"
msgstr ""

#: static/js/script.js
msgid "report_header_DNSSEC"
msgstr ""

#: static/js/script.js
msgid "report_header_ftp"
msgstr ""

#: static/js/script.js
msgid "report_header_tls_qualys_certificate_trusted"
msgstr ""

#: static/js/script.js
msgid "report_header_tls_qualys_encryption_quality"
msgstr ""

#: static/js/script.js
msgid "category_menu_municipality"
msgstr ""

#: static/js/script.js
msgid "category_menu_cyber"
msgstr ""

#: static/js/script.js
msgid "category_menu_unknown"
msgstr ""

#: static/js/script.js
msgid "category_menu_water_board"
msgstr ""

#: static/js/script.js
msgid "category_menu_province"
msgstr ""

#: static/js/script.js
msgid "category_menu_hacking"
msgstr ""

#: static/js/script.js
msgid "category_menu_country"
msgstr ""

#: static/js/script.js
msgid "category_menu_region"
msgstr ""

#: static/js/script.js
msgid "category_menu_county"
msgstr ""

#: static/js/script.js
msgid "category_menu_district"
msgstr ""

#: static/js/script.js
msgid "category_menu_government"
msgstr ""

#: static/js/script.js
msgid "category_menu_healthcare"
msgstr ""

#: static/js/script.js
msgid "category_menu_finance"
msgstr ""

#: static/js/script.js
msgid "category_menu_state"
msgstr ""

#: static/js/script.js
msgid "category_menu_school"
msgstr ""

#: static/js/script.js
msgid "Strict-Transport-Security"
msgstr ""

#: static/js/script.js
msgid "X-Content-Type-Options"
msgstr ""

#: static/js/script.js
msgid "X-Frame-Options"
msgstr ""

#: static/js/script.js
msgid "X-XSS-Protection"
msgstr ""

#: static/js/script.js
msgid "plain_https"
msgstr ""

#: static/js/script.js
msgid "tls_qualys"
msgstr ""

#: static/js/script.js
msgid "DNSSEC"
msgstr ""

#: static/js/script.js
msgid "ftp"
msgstr ""

#: static/js/script.js
msgid "tls_qualys_encryption_quality"
msgstr ""

#: static/js/script.js
msgid "tls_qualys_certificate_trusted"
msgstr ""

#: static/js/script.js
msgid "category_menu_bundesland"
msgstr ""

#: static/js/script.js
msgid "category_menu_regierungsbezirk"
msgstr ""

#: static/js/script.js
msgid "category_menu_landkreis_kreis_kreisfreie_stadt"
msgstr ""

#: static/js/script.js
msgid "category_menu_samtgemeinde_verwaltungsgemeinschaft"
msgstr ""

#: static/js/script.js
msgid "category_menu_stadt_gemeinde"
msgstr ""

#: static/js/script.js
msgid "category_menu_stadtbezirk_gemeindeteil_mit_selbstverwaltung"
msgstr ""

#: static/js/script.js
msgid "category_menu_stadtteil_gemeindeteil_ohne_selbstverwaltung"
msgstr ""

#: static/js/script.js
msgid "One week earlier"
msgstr ""

#: static/js/script.js
msgid "One week later"
msgstr ""

#: static/js/script.js
msgid "Moment"
msgstr ""

#: static/js/script.js
msgid "Risks"
msgstr ""

#: static/js/views.js
msgid "week"
msgstr ""

#: static/js/views.js
msgid "Second opinion"
msgstr ""

#: static/js/views.js
msgid "Documentation"
msgstr ""

#: static/js/views.js
msgid "Explanation of finding"
msgstr ""

#: static/js/views.js
msgid ""
"Hi!,\n"
"\n"
"I would like to explain the below finding.\n"
"\n"
"Address: {{ url }}\n"
"Scan Type: {{ scan_type }}\n"
"Scan ID: {{ scan_id }}\n"
"Impact: High: {{ high }}, Medium {{ medium }}, Low: {{ low }}.\n"
"\n"
"I believe the finding to be incorrect. This is why:\n"
"[... please enter your explanation for review here ...]\n"
"\n"
"I acknowledge that this finding will be published together with my "
"organizations name.\n"
"\n"
"tip: please refer to documentation or standards where possible. Be aware "
"that an explanation is valid for one year by default.\n"
"\n"
"Kind regards,\n"
msgstr ""

#: static/js/views.js
msgid "score perfect"
msgstr ""

#: static/js/views.js
msgid "score high"
msgstr ""

#: static/js/views.js
msgid "score medium"
msgstr ""

#: static/js/views.js
msgid "score low"
msgstr ""

#: static/js/views.js
msgid "Exit Full Screen"
msgstr ""
